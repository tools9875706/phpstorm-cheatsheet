=== .gitignore
- We can only ignore *unversioned* files.
- If we use automatically add new files and we check `Don't ask again`, there will be no more `add to gitignore` in the context menu.

=== unversion a versioned file
.in the project open terminal
[source, shell]
----
git rm --cached my_filename.txt
----

=== Add to gitignore
- only if the file color is *red* (not added to git yet), we can add to gitignore
- manually copying the file path to the .gitignore file (not recommended)
- from the context menu (right click on the file)

=== Disabling auto adding files to git
- `Settings > Version Control > Confirmation > When files are added: Ask`


<<<
