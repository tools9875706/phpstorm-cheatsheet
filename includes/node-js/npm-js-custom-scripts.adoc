=== NPM custom scripts
Custom scripts can be run with one click, after selected

*1. Add a custom script into the `package.json` file*

.example
----
"my-custom-script": "ng serve --hmr --open"
----

*2. Add custom run configuration*

- click in toolbar `Run > Edit Configursations...`
- click the `+` icon to add new configuration
- select `npm`

and setup the fields:

- `Name:` add a custom name you want
- `package.json:` add the package.json location (if you stay in the project, it is automatic)
- `Command:` should be `run`
- `Scripts:` select your custom script from the dropdown, for example: `my-custom-script`
- no need to configure the remaining fields

=== Running multiple / concurrent scripts with Environment Variables (best option)

In the tools window click in toolbar `Run > Edit Configursations...`

image::../../assets/node-js/node-js-custom-scripts.png[]

click the document icon on the right side of the `Environment` field:
image:../../assets/node-js/node-js-environment-document-icon.png[]

then:

image::../../assets/node-js/node-js-environment-variables.png[]

paste the command by clicking tha paste icon:
image:../../assets/node-js/node-js-environment-paste-icon.png[]

=== Running multiple / concurrent scripts with concurrently (alternative option)
.Install concurrently
----
npm install --save-dev concurrently
----

.Add a new script in your package.json
----
"my-custom-script": "concurrently \"ng serve --hmr --open\" \"npm run watch\""
----





<<<